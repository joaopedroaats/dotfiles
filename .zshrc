source ~/dotfiles/.zsh/config.zsh    # 1
source ~/dotfiles/.zsh/aliases.zsh   # 2
source ~/dotfiles/.zsh/spaceship.zsh # 3
source ~/dotfiles/.zsh/zinit.zsh     # 4
source ~/dotfiles/.zsh/antigen.zsh   # 4
