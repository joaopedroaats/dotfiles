echo "========================================== Xfce (1) ====================="
yay -S xfce4-settings --noconfirm

echo "========================================== Compositor (1) ====================="
yay -S picom --noconfirm

echo "========================================== Documents (1) ====================="
yay -S evince --noconfirm

echo "========================================== Menu (3) ====================="
yay -S dmenu rofi rofi-calc --noconfirm

echo "========================================== Lightdm (4) ====================="
yay -S lightdm lightdm-gtk-greeter lightdm-gtk-greeter-settings lightdm-webkit2-greeter --noconfirm

echo "========================================== Filemanager (6) ====================="
yay -S thunar thunar-archive-plugin thunar-volman tumbler gvfs ranger-git --noconfirm

echo "========================================== Desktop (2) ====================="
yay -S nitrogen lxappearance --noconfirm

echo "========================================== Unpackfiles (6) ====================="
yay -S xarchiver p7zip zip unzip unrar unace --noconfirm

echo "========================================== Console (3) ====================="
yay -S kitty alacritty-git xcompmgr --noconfirm

echo "========================================== Themes (1) ====================="
yay -S ant-dracula-gtk-theme --noconfirm

echo "========================================== Themes / Icons (2) ====================="
yay -S tela-icon-theme-git flatery-icon-theme-git --noconfirm

echo "========================================== Themes / Cursors (1) ====================="
yay -S capitaine-cursors --noconfirm

echo "========================================== Media / Image (4) ====================="
yay -S feh imagemagick flameshot-git gcolor2 --noconfirm

echo "========================================== Media / Video (1) ====================="
yay -S mplayer --noconfirm

echo "========================================== Audio / Pulseaudio (3) ====================="
yay -S pulseaudio pulseaudio-alsa pavucontrol --noconfirm

echo "========================================== Audio / Alsa (4) ====================="
yay -S alsa-firmware alsa-lib alsa-plugins alsa-utils --noconfirm

echo "========================================== Audio / Icon (1) ====================="
yay -S volumeicon --noconfirm

echo "========================================== Audio / Cli (1) ====================="
yay -S playerctl --noconfirm

