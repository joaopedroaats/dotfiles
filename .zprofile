# Oh My Zsh
export ZSH="/home/joaopedro/.oh-my-zsh"

# Nvm
export NVM_DIR="$HOME/.nvm"

# Android Studio
export ANDROID_HOME="$HOME/Android/Sdk"
export PATH="$PATH:$ANDROID_HOME/tools"
export PATH="$PATH:$ANDROID_HOME/platform-tools"

# PATH
export PATH="$PATH:$HOME/.config/composer/vendor/bin"
export PATH="$PATH:$HOME/.config/rofi/bin"

export PATH="$PATH:$HOME/.deno/bin"
export PATH="$PATH:$HOME/.emacs.d/bin"
export PATH="$PATH:$HOME/.rvm/bin"
export PATH="$PATH:$HOME/.cabal/bin"

export PATH="$PATH:$HOME/go/bin"
export PATH="$PATH:$HOME/flutter/bin"
export PATH="$PATH:$HOME/flutter/bin/cache/dart-sdk/bin"
export PATH="$PATH:$HOME/anaconda3/bin"
